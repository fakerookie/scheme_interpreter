from debugs import main, trace, interact
from operator import add, sub, mul, truediv
from reader import Pair, nil, read, buffer_input

def simplify(value):
    if isinstance(value, float) and int(value) == value:
        return int(value)
    return value

def reduce(f, lst, start):
    if lst is nil:
        return start
    return reduce(f, lst.second, f(start, lst.first))

def apply(operator, args):
    if not isinstance(operator, str):
        raise TypeError(str(operator) + ' is not a symbol')

    if operator == '+':
        return reduce(add, args, 0)
    elif operator == '-':
        if len(args) == 0:
            raise TypeError(operator + ' require at least ONE argument')
        elif len(args) == 1:
            return -args.first
        else:
            return reduce(sub, args.second, args.first)
    elif operator == '*':
        return reduce(mul, args, 1)
    elif operator == '/':
        if len(args) == 0:
            raise TypeError(operator + ' require at least ONE argument')
        elif len(args) == 1:
            return 1 / args.first
        else:
            return reduce(truediv, args.second, args.first)
    else:
        raise TypeError(operator + ' is an unknow operator')

def eval(exp):
    if type(exp) in (int, float):
        return simplify(exp)
    elif isinstance(exp, Pair):
        args = exp.second.map(eval)
        return simplify(apply(exp.first, args))
    else:
        raise TypeError(str(exp) + ' is not a number or expression')

def scm_lst(*args):
    if len(args) == 0:
        return nil
    return Pair(args[0], scm_lst[*args[1:]])

@main
def repl():
    while True:
        try:
            src = buffer_input()
            while src.more_on_lines:
                exp = read(src)
                print(eval(exp))
        except (SyntaxError, TypeError, ValueError, ZeroDivisionError) as err:
            print(type(err).__name__ + ": ", err)
        except (KeyboardInterrupt, EOFError):
            print("Calculation completed.")
            return