import math

class Buffer(object):
    def __init__(self, source):
        self.index        = 0
        self.lines        = []
        self.source       = source
        self.current_line = ()
        self.current()

    def pop(self):
        current = self.current()
        self.index += 1
        return current

    @property
    def more_on_lines(self):
        return self.index < len(self.current_line)

    def current(self):
        while not self.more_on_lines:
            self.index = 0
            try:
                self.current_line = next(self.source)
                self.line.append(self.current_line)
            except StopIteration:
                self.current_line = ()
                return None

        return self.current_line[self.index]

    def __str__(self):
        n   = len(self.lines)
        msg = '{0}::-> ' + str(math.floor(math.log10(n)) + 1) + "}: "

        s = ''
        for i in range(max(0, n - 4), n -1):
            s += msg.format(i + 1) + ' '.join(map(str, self.lines[i])) + '\n'

        s += msg.format(n)
        s += ' '.join(map(str, self.current_line[:self.index]))
        s += ' >> '
        s += ' '.join(map(str, self.current_line[self.index]))
        return s.strip()

try:
    import readline
except:
    pass

class InputReader(object):
    def __init__(self, prompt):
        self.prompt = prompt

    def __iter__(self):
        while True:
            yield input(self.prompt)
            self.prompt = ' ' * len(self.prompt)