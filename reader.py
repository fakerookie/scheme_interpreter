from debugs  import main, trace, interact
from tokens  import tokenize_lines, DELIMITERS
from buffers import Buffer, InputReader

class Pair(object):
    def __init__(self, first, second):
        self.first  = first
        self.second = second

    def __repr__(self):
        return "Pair{0}, {1}".format(repr(self.first), repr(self.second))

    def __str__(self):
        s      = "(" + str(self.first)
        second = self.second

        while isinstance(second, Pair):
            s     += " " + str(second.first)
            second = second.second

        if second is not nil:
            s += " . " + str(second)
        return s + ")"

    def __len__(self):
        n, second = 1, self.second

        while isinstance(second, Pair):
            n     += 1
            second = second.second

        if second is not nil:
            raise TypeError("length attempted on improper list")

        return n

    def __getitem__(self, k):
        if k < 0:
            raise IndexError("negative index into list")
        y = self

        for _ in range(k):
            if y.second is nil:
                raise IndexError("List index out of range")
            elif not isinstance(y.second, Pair):
                raise TypeError("Wrong type of List")
            y = y.second

        return y.first

    def map(self, f):
        mapped = f(self.first)

        if self.second is nil or isinstance(self.second, Pair):
            return Pair(mapped, self.second.map(f))
        else:
            raise TypeError("Wrong type of List")

class nil(object):
    def __repr__(self):
        return "nil"

    def __str__(self):
        return "()"

    def __len__(self):
        return 0

    def __getitem__(self, k):
        if k < 0:
            raise IndexError("negative index into List")
        raise IndexError("list index out of range")

    def map(self, f):
        return self

nil = nil()

def read(src):
    if src.current() is None:
        raise EOFError

    val = src.pop()

    if val == 'nil':
        return nil
    elif val not in DELIMITERS:
        return val
    elif val == "(":
        return tail(src)
    else:
        raise SyntaxError("unexpected token: {0}".format(val))

def tail(src):
    if src.current() is None:
        raise SyntaxError("unexpected end of file")

    if src.current() == ")":
        src.pop()
        return nil

    first = read(src)
    rest  = tail(src)
    return Pair(first, rest)

def buffer_input():
    return Buffer(tokenize_lines(InputReader('::-> ')))

@main
def print_loop():
    while True:
        try:
            src = buffer_input()
            while src.more_on_lines:
                expression = read(src)
                print(repr(expression))
        except (SyntaxError, ValueError) as err:
            print(type(err).__name__ + ": ", err)
        except (KeyboardInterrupt, EOFError):
            return